<?php
class Activite
{
    // ATTENTION: seulement les propriétés essentielles pour l'affichage dans le tableau de semaine.php sont codées

    // VL La date sera de stype string pour le moment
    // revenir sur le type de cette propriété (voir get/set)
    public string $date;
    public string $activite;
    public int $intensite;
    public string $determinants;
    public int $duree;

    /**
     * Méthode d'accès qui permet de récupérer la valeur dans la propriété $date.
     * @return string : valeur de $date
     */
    public function getDate() : String
    {
        return $this->date;
    }

    /**
     * Méthode d'accès pour donner une nouvelle valeur à la propriété $date
     * @param array $date
     */
    public function setDate(array $date): void
    {
        $this->date = $date;
    }

    /**
     * Méthode d'accès qui permet de récupérer la valeur dans la propriété $activite.
     * @return string : valeur de $activite
     */
    public function getActivite() : string
    {
        return $this->activite;
    }

    /**
     * Méthode d'accès pour donner une nouvelle valeur à la propriété $activite
     * @param string $activite
     */
    public function setActivite($activite): void
    {
        $this->activite = $activite;
    }

    /**
     * Méthode d'accès qui permet de récupérer la valeur dans la propriété $determinants.
     * @return string : valeur de $determinants
     */
    public function getDeterminants() : string
    {
        return $this->determinants;
    }

    /**
     * Méthode d'accès pour donner une nouvelle valeur à la propriété $determinants
     * @param string $determinants
     */
    public function setDeterminants($determinants): void
    {
        $this->determinants = $determinants;
    }

    /**
     * Méthode d'accès qui permet de récupérer la valeur dans la propriété intensite.
     * @return int : valeur de intensite
     */
    public function getIntensite() : int
    {
        return $this->intensite;
    }

    /**
     * Méthode d'accès pour donner une nouvelle valeur à la propriété $intensite
     * @param int $intensite
     */
    public function setIntensite($intensite): void
    {
        $this->intensite = $intensite;
    }

    /**
     * Méthode d'accès qui permet de récupérer la valeur dans la propriété $duree.
     * @return int : valeur de $duree
     */
    public function getDuree()
    {
        return $this->duree;
    }

    /**
     * Méthode d'accès pour donner une nouvelle valeur à la propriété $duree
     * @param int $duree
     */
    public function setDuree($duree): void
    {
        $this->duree = $duree;
    }

    /**
     * Constructeur d'Activite. Il reçoit une liste contenant les infos entrés dans les champs du formulaire.
     * Pour chaque propriété, le constructeur vérifie dans la liste s'il y a une valeur ayant comme clé le nom de la propriété.
     * S'il n'y en a pas, on met des valeurs par défaut pour cette clé.
     *
     * @param array $activiteDetails : champs remplis dans ajoutActivite.php
     */

    public function __construct(array $activiteDetails)
    {
        $this->date = isset($activiteDetails["date"]) ? $activiteDetails["date"] : "";
        $this->activite = isset($activiteDetails["activite"]) ? $activiteDetails["activite"] : "Une activité";
        $this->determinants = isset($activiteDetails["determinants"]) ? $activiteDetails["determinants"] : "cardio";
        $this->intensite = isset($activiteDetails["intensite"]) ? $activiteDetails["intensite"] : "3";
        $this->duree = isset($activiteDetails["duree"]) ? $activiteDetails["duree"] : 30;
    }

    // REVENIR pour faire tostring si nécessaire
    public function __toString():string
    {
        $activite = "$this->nom : [";
        foreach ($this->joueurs as $joueur){
            $activite.= "$joueur->nom";
        }
        return $activite."]";
    }

}