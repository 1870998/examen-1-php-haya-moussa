        <footer class="container text-right">
            <address>
                333, boul. de la Cité-des-Jeunes<br>
                Gatineau, secteur Hull<br>
                (Québec) Canada  J8Y 6M4<br>
                <a href="mailto:vieactive@cegepoutaouais.qc.ca">vieactive@cegepoutaouais.qc.ca</a>
            </address>
        </footer>
    </body>
</html>