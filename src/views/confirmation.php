<?php
//VL il faut démarrer la session avant les entêtes (avant la production de HTML)
// Démarrer la session
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

require_once ("../controllers/creerActivite.php");
include("templates/header.php");

print_r($_SESSION);
?>
<main class="container-md">
    <!-- Fil d'ariane -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent">
            <li class="breadcrumb-item"><a href="#">Journal</a>
            <li class="breadcrumb-item"><a href="#">Cours 1</a></li>
            <li class="breadcrumb-item"><a href="semaine.php">Semaine 1</a></li>
            <li class="breadcrumb-item active" aria-current="page">Ajouter une activité</li>
        </ol>
    </nav>

    <?php

        //@TODO : appeller le contrôleur pour ajouter l'activité
        //VL $_POST["activites"] n'existe pas; tu peux regarder avec  print_r($_POST);
        if(isset($_POST))
        {
            $activite = false;

            // Entreposer le tableau activite dans session pour l'accéder de partout
            // VL ce bout de code empêche le stockage de plusieurs activités; $_SESSION["activites"] devraient contenir l'ensemble des activités
            // $_SESSION["activites"] =  $_POST["activites"];

            // Appeler la méthode statique ajoutNouvelleActivite du controlleur pour créer une activité et nous retourner un bool nous disant si cela a été accompli
            $activite = creerActivite::ajoutNouvelleActivite($_POST);
            // Si l'activité a été créee, la méthode creerUnJoueur retourne true et on affiche
            if($activite){
                echo '<p class="display-4 mt-4">L\'activité a été ajoutée à votre journal</p>';
            }
            // Si l'activité n'a pas été créée, on affiche un message d'erreur
            else
            {
                echo '<p class="display-4 mt-4">Désolé... L\'activité n\'a pas pu être ajoutée à votre journal</p>';
            }
        }
    ?>
    <a class="btn btn-primary mt-4" href="ajoutActivite.php">Ajouter une autre activité</a>
</main>
<?php include("templates/footer.php") ?>