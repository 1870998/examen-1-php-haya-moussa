<?php
require_once "./../models/Activite.php";
// VL Il faut démarrer la session avant l'envoi des entêtes
// Démarrer la session
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
include("templates/header.php");

// J'ai des choses qui sont dans la session, mais je n'arrive pas à y accéder
// print_r($_SESSION);

?>
<main class="container">
    <!-- Fil d'ariane -->
    <nav aria-label="breadcrumb">
        <ol class="breadcrumb bg-transparent">
            <li class="breadcrumb-item"><a href="#">Journal</a>
            <li class="breadcrumb-item"><a href="#">Cours 1</a></li>
            <li class="breadcrumb-item active" aria-current="page">Semaine 1</li>
        </ol>
    </nav>
    <!-- Titre de la page -->
    <h1 class="h3 mb-4">Semaine 1 (10 février au 14 février)</h1>
    <!-- Liste des activités -->
    <section class="container mt-3">
        <h2 class="h4">Liste des activités</h2>
        <!-- Tableau  -->
        <!-- @TODO Compléter le tableau avec les activités ajoutées -->
        <div class="table-responsive">
            <table class="table">
                <caption class="d-none">Liste des activités</caption>
                <thead>
                <tr>
                    <th scope="col">Date</th>
                    <th scope="col">Activités</th>
                    <th scope="col">Intensité</th>
                    <th scope="col">Déterminants</th>
                    <th scope="col">Durée (min)</th>
                </tr>
                </thead>
                <tbody>

                <?php
                // Mettre une durée totale de 0 avant de commencer à boucler
                $dureeTotale = 0;

                // Vérifier si on a une liste d'activités
                if (isset($_SESSION["activites"]))
                {

                    // Boucler chaque activité dans la liste
                    foreach ($_SESSION["activites"] as $activite)
                    {
                        // Créer une rangée pour l'activité courante
                        echo "<tr>";
                        // Faire une case pour chaque propriété de l'activité
                        $date = $activite->getDate();
                        echo "<td>".$date."</td>";
//                        echo "<td>".$activite["date"]."</td>";

                        $nomActivite = $activite->getActivite();
                        echo "<td>".$nomActivite."</td>";
//                        echo "<td>".$activite["activite"]."</td>";

                        $intensite = $activite->getIntensite();
                        echo "<td>".$intensite."</td>";
//                        echo "<td>".$activite["intensite"]."</td>";

                        $determinants = $activite->getDeterminants();
                        echo "<td>".$determinants."</td>";
//                        echo "<td>".$activite["determinants"]."</td>";

                        $duree = $activite->getDuree();
                        echo "<td>".$duree."</td>";
//                        echo "<td>".$activite["duree"]."</td>";

                        // Fermer la rangée pour l'activité
                        echo "<tr>";

                        // Additionner la durée de l'activité courante au totale
                        $dureeTotale.$duree;
                    }
                }
                ?>
                </tbody>
                <tfoot>
                    <tr>
                        <!-- @TODO Modifier la durée totale affichée pour le total des durées dans le tableau -->
                        <td class="text-right" colspan="5"><strong>Total :</strong> <span id="dureeTotale"><?php $dureeTotale?></span> min</td>
                    </tr>
                </tfoot>
            </table>
        </div>
        <a href="ajoutActivite.php" class="btn btn-outline-primary">Ajouter une activité</a>
    </section>
    <!-- Évaluation de la pratique -->
    <section class="container mt-3">
        <h2 class="h4 mb-3">Évaluation de ma pratique</h2>
        <form>
            <div class="form-group">
                <label for="responsable">Je suis responsable de ma pratique d'activités physiques et de mes apprentissages à l'extérieur du cours.</label>
                <!-- Ne pas se préoccuper de l'entrée de la valeur pour l'instant -->
                <input type="number" class="form-control d-none" id="responsable" value="3" min="0" max="5" step="0.5" required>
                <div class="ml-2">
                    <i id="responsable1" class="fas fa-star fa-lg"></i>
                    <i id="responsable2" class="fas fa-star fa-lg"></i>
                    <i id="responsable3" class="fas fa-star-half-alt fa-lg"></i>
                    <i id="responsable4" class="far fa-star fa-lg"></i>
                    <i id="responsable5" class="far fa-star fa-lg"></i>
                </div>
            </div>
            <div class="form-group">
                <label for="suffisant">Je m'organise, je pratique l'activité physique de maniere suffisante et réguliere.</label>
                <!-- Ne pas se préoccuper de l'entrée de la valeur pour l'instant -->
                <input type="number" class="form-control d-none" id="suffisant" value="3" min="0" max="5" step="0.5" required>
                <div class="ml-2">
                    <i id="suffisant1" class="fas fa-star fa-lg"></i>
                    <i id="suffisant2" class="fas fa-star fa-lg"></i>
                    <i id="suffisant3" class="fas fa-star fa-lg"></i>
                    <i id="suffisant4" class="far fa-star fa-lg"></i>
                    <i id="suffisant5" class="far fa-star fa-lg"></i>
                </div>
            </div>
        </form>
    </section>
    <!-- L9 : Boutons -->
    <section class="container text-center">
        <button class="btn btn-primary mb-5" type="submit">Enregistrer et envoyer</button>
    </section>
</main>
<?php include("templates/footer.php") ?>
