<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require_once __DIR__ . "/../models/Activite.php";

class creerActivite
{
    //VL le & permet de retourner l'adresse du tableau d'activités et de l'utiliser avec le code plus bas (autres options possibles ex dans le solutionnaire)
    private static function &getActivites(): array
    {
        // Créer une nouveau tableau qui contiendra les activités s'il n'existe pas
        if (!isset($_SESSION["activites"])) {
            $_SESSION["activites"] = [];
        }
        return $_SESSION["activites"];
    }

    //VL /* empêchait l'activation du bloc PHPDoc
    //TODO Compléter la méthode de validation

    /**
     * Cette fonction permet de valider une chaine de caractère string reçus en paramètre.
     * La chaine peut seulement contenir des lettres (majuscule ou minuscules) et les symboles suivants : , ; - '
     * La valeur de retour est true si la chaine respecte ce format et retourne false si elle ne respecte pas ce format.
     * La fonction est utilisés pour valider tous les champs du formulaire dans ajoutAcitivite.php
     *
     * @param string $chaine : valeur du  formulaire ajoutActivite.php
     * @return bool : true si valide, false si invalide
     */
    public static function estValide(string $chaine): bool
    {
        //VL Ajouter l'espace
        if(preg_match('/^[A-Za-z, ;\'-]+$/', $chaine))
        {
            // Retourne true et sors de la méthode
            return true;
        }
        // Retourne false si la chaine n'est pas valide
        return false;
    }


    public static function ajoutNouvelleActivite(array $details): bool
    {
        $creation = false;
        //@TODO Ajouter les instructions pour valider les données de l'activité ($details) à l'aide de la méthode estValide

        if(self::estValide($details["lieu"]) == true)
        {
            if(self::estValide($details["partenaires"]) == true)
            {
                if(self::estValide($details["activite"]) == true)
                {
                    if(self::estValide($details["determinants"]) == true)
                    {
                        if(self::estValide($details["motivation"]) == true)
                        {
                            // Si toutes les validations de types texte sont bonnes, créer l'activité
                            $activite = new Activite($details);

                            //@TODO Si valide, l'ajouter au tableau des activités
                            // Créer avec la méthode un tableau dans la session ayant la clé activites (si elle n'existe pas déjà)
                            // activites contient $_SESSION["activites"] VL : son contenu; & permet de stocker son adresse (passage par référence)
                            $activites = &self::getActivites(); //Retourne le contenu du tableau et non la référence au tableau de session

                            // Ajouter la nouvelle activité à la fin de la liste des activites
                            array_push($activites, $activite);

                            // True sera retournée s'il y a plus d'une activité dans la liste
                            if (count($activites)>0)
                            {
                                $creation = true;
                                return $creation;
                            }
                        }
                    }
                }
            }
        }
        //Retourner vrai si l'activité a été ajoutée, faux sinon
        return $creation;
    }
}