<?php
require_once __DIR__ . "/../../src/controllers/creerActivite.php";

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}

use PHPUnit\Framework\TestCase;

class creerActiviteTest extends TestCase
{
    static function setUpBeforeClass(): void
    {
        $_SESSION = array();
    }
    //@TODO implémenter ce test
    public function testEstValideChaineValide():void
    {
        $this->assertEquals(true, creerActivite::estValide("Nathalie, Jean-Marc et 'tite laine"));
    }

    //@TODO implémenter ce test
    public function testEstValideChaineInValide():void
    {
        $this->assertEquals(false, creerActivite::estValide("33 Cité-des-Jeunes"));
    }
    public function tearDown(): void
    {
        $_SESSION = array();
    }
}
